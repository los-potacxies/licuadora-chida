import React from 'react'
import './navbar.css';

function NavBar() {
    return (
        <nav className="nav-bar">
            <a href="/" rel="noopener noreferrer">
                <h3 className="logo">App</h3>
            </a>
            <a href="/" rel="noopener noreferrer" className='link'>Inicio</a>
            <a href="/" rel="noopener noreferrer" className='link'>Productos</a>
            <a href="https://neave.tv/" target="_blank" rel="noopener noreferrer" className='link'>¡Sorpresa!</a>
        </nav>
    )
}

export { NavBar }